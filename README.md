# Asana WebDev Take-Home Exercise


## Instructions -- by Kristin Burton

Thanks for the opportunity! For this project, I focused on using vanilla JS, CSS, and HTML with some additional tools. I used Node.js, npm, jQuery and handlebars to help supplement the project. To view my production ready work (/dist/), use npm browerSync to spin up a web server.


## **Growth Focus:** 
## Guidance from the design team

 **"These images should be displayed on the page as a set of thumbnails; clicking on a thumbnail should display a full-sized image to the user."** 

I used a masonry style with CSS columns for ease of adding dynamically generated images from the JSON. I kept it simple for this exercise to show my CSS skill set. But for a larger and more complex project, I would consider using a masonry library for preciseness.

 **"Full-size photos should be able to be closed, and the user should be returned to the thumbnail gallery."** 

There are many good libraries out there for generating a gallery lightbox that have very clean UI/UX but for this project I decided to code a simple image modal for the thumbnails to showcase my JavaScript skills. It’s very lightweight, which is key, since the site is image heavy.


## Requirements from the adoption team

 **"Starting next month, we'll be partnering with a shelter that has 2000 pets available for adoption. We need this page to be able to handle that many listings."** 

I opted to use a lazy load for the thumbnails to ease the amount of resources being downloaded at once. Due to not knowing who generates and delivers the JSON file, I did not create responsive image files for different devices since the path of the image is in the JSON file. Creating the responsive images and using the srcset attribute would be a great implementation for a site with over 2000 pet images.

 **"Most people find out about us from internet searches. We need this page to appear towards the top of search results."** 

I included a meta title, meta description, caption for the image, alt text and updated the h1 to help improve SEO. I also optimized the images with gulp imagemin to reduce the file sizes. 

It would be great to gather more content on each dog and include it in the JSON file to help with SEO as well as more body content.
