var dogsGallery ={

  handlerData:function(resJSON){

    var templateSource   = $('#dogs-gallery-template').html(),
    template = Handlebars.compile(templateSource),
    dogsHTML = template(resJSON);

    $('#dogs-gallery').html(dogsHTML);

    $('.lazy').Lazy({
        effect: 'fadeIn',
        effectTime: 5000,
        threshold: 0
      });
      
  },
  loadDogsData : function(){
      $.ajax({
          url: 'data/dogs.json',
          method:'get',
          success:this.handlerData
      })
  }
};

$(document).ready(function(){
    dogsGallery.loadDogsData();
});

document.addEventListener('click', function (event) {
  if (!event.target.matches('img.dog-thumbnail')) return;

  else if (event.target.matches('img.dog-thumbnail')) {
    var target = $(event.target);
    $('.modal-image').attr('src', target.attr('src'));
    $('.modal-link').attr('href', target.data('caption'));
    $('body').addClass('noScroll');
    $('#myModal').show(); 
  }
  
}, false);


document.addEventListener('click', function (event) {
    if (!event.target.matches('span.close')) return;

    else if (event.target.matches('span.close')) {
      $('body').removeClass('noScroll');
      $('#myModal').hide(); 
    }

}, false);

document.addEventListener('click', function (event) {
  if (!$(event.target).closest('.modal-content,img.dog-thumbnail').length) {
    $('body').removeClass('noScroll').find('.modal').hide();
  }
}, false);